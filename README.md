# Using a private image from Gitlab

This is a test repository for pulling an image from a gitlab registry that requires authentication.

This requires that a repository is configured as a [project](https://www.gitpod.io/docs/configure/projects) with an [environment variable](https://www.gitpod.io/docs/configure/projects/environment-variables) that contains credentials access that repository.

The variable name must be GITPOD_IMAGE_AUTH and have a value set as:<br>
registry.gitlab.com:"base64 encoded string of username:password"

Refer to `secret_maker.sh` for an quick way to generate that string:
```bash
$ ./secret_maker.sh registry.gitlab.com mrzarquon mysecretoken
Set the following as a project level environment variable to enable using private images from registry.gitlab.com
GITPOD_IMAGE_AUTH = "registry.gitlab.com:bXJ6YXJxdW9uOm15c2VjcmV0b2tlbgo="
```

## Minimal CI Job to create this in Gitlab

This is the barebones example of a build job to create and push a container in gitlab CI:

```yaml
redhat-8:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/redhat-8.Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:redhat-8-$(date -I)"

```
